package com.systex.dao;

import com.systex.model.Task;

import java.util.List;

public interface TaskDao {
    void addTask(Task p);
    List<Task> listTasks();
    Task getTaskById(int id);
    void removeTask(int id);
}
