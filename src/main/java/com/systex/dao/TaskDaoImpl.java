package com.systex.dao;

//負責與DB的存取
//負責與數據庫進行交互設計，用來處理數據的持久化工作。DAO層的設計首先是設計DAO的接口
//建立了DAO層後才可以建立Service層

import com.systex.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;


@Repository
public class TaskDaoImpl implements TaskDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addTask(Task t) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(t);
    }

    @Override
    public List<Task> listTasks() {
        Session session = this.sessionFactory.getCurrentSession(
        );
        List<Task> tasksList = session.createQuery("from Task").list();
        return tasksList;
    }

    @Override
    public Task getTaskById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Task t = (Task) session.load(Task.class, id);
        return t;
    }

    @Override
    public void removeTask(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Task t = (Task) session.load(Task.class, id);
        if (null != t) {
            session.delete(t);
        }
    }
}