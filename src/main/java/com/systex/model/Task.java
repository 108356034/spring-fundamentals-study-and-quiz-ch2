package com.systex.model;

import javax.persistence.*;

//(Model) 負責table相關設定
//每個模型都有一個Service接口，每個接口分別封裝各自的業務處理方法
@Entity
@Table(name = "task_table")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_seq")
    private int id;
    @Column(name = "task_name")
    private String name;
    @Column(name = "task_description")
    private String description;

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TaskTable [id=" + id + ", name=" + name + ", review= " + description + "]";
//
//        return "task_table{" +
//                ", task_name='" + name + '\'' +
//                ", task_description='" + description +
//                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Task other = (Task) obj;
        if (id != other.id) return false;
        return true;
    }


}
