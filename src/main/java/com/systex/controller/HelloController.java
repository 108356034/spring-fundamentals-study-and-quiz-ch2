package com.systex.controller;

import com.systex.model.Task;
import com.systex.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloController {
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/")
    public String listTasks(Model model) {
        model.addAttribute("task", new Task());
        model.addAttribute("listTasks", this.taskService.listTasks());
        return "index";
    }

    @GetMapping(value = "/add")
    public String addTask(@RequestParam("task_name") String task_name, @RequestParam("task_description") String task_description) {
        this.taskService.addTask(new Task(task_name, task_description));
        return "redirect:/";
    }

    @GetMapping("/remove/{task_seq}")
    public String removeTask(@PathVariable("task_seq") int task_seq) {
        this.taskService.removeTask(task_seq);
        return "redirect:/";
    }
}