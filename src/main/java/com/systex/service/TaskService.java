package com.systex.service;

import com.systex.model.Task;

import java.util.List;

public interface TaskService {
    List<Task> listTasks();
    Task getTaskById(int id);
    void removeTask(int id);
    void addTask(Task p);
}