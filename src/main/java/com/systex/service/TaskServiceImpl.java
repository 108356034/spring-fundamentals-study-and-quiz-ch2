package com.systex.service;

//負責於controller中提供服務
//建立了DAO層後才可以建立Service層
//Service層又是在Controller層之下
//既調用DAO層的接口，又要提供接口給Controller層的類來進行調用，它剛好處於一個中間層的位置

import java.util.List;

import com.systex.dao.TaskDao;
import com.systex.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("taskService")
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDao taskDao;

//    @Autowired
//    public void setTaskDAO(TaskDao taskDao) {
//        this.taskDao = taskDao;
//    }

    @Override
    @Transactional
    public List<Task> listTasks() {
        return this.taskDao.listTasks();
    }

    @Override
    @Transactional
    public Task getTaskById(int id) {
        return this.taskDao.getTaskById(id);
    }

    @Override
    @Transactional
    public void removeTask(int id) {
        this.taskDao.removeTask(id);
    }

    @Override
    @Transactional
    public void addTask(Task task) {
        this.taskDao.addTask(task);
    }
}
