<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>spring-fundamentals-study-and-quiz-ch2</title>
</head>
<body>
    <h1>Task List</h1>
    <c:if test="${!empty listTasks}">
        <table border = 1 >
        <tr>
            <th>Task_Seq</th>
            <th>Task_Name</th>
            <th>Description</th>
            <th>Delete</th>
        </tr>
        <c:forEach items="${listTasks}" var="task">
            <tr>
                <td>${task.getId()}</td>
                <td>${task.getName()}</td>
                <td>${task.getDescription()}</td>
                <td><a href="<c:url value='/remove/${task.getId()}'/>">Delete</a></td>
            </tr>
        </c:forEach>
        </table>
    </c:if>
    <br>
    <h1>Add New Task</h1>
    <form action="add">
      <div>Task Name: <input type = "text" name = "task_name" required="required"/></div>
      <div>Description: <input type = "text" name = "task_description"/></div>
      <div><input type = "submit" value="save"/></div>
    </form>
</body>
</html>